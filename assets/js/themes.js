const themeCookieTTL = 365 * 10;

function fromURL() {
  const urlParams = getUrlParams();
  if (urlParams.hasOwnProperty('_theme')) {
    document.body.className = urlParams['_theme'];
    setCookie('theme', themeName, themeCookieTTL);
  }
}

function fromCookie() {
  let themeName = getCookie('theme');

  if (null !== themeName) {
   document.body.className = themeName;
  }
}

function fromButton() {
  const switchers = document.getElementsByClassName('theme-switcher');

  for (let i = 0, len = switchers.length; i < len; i++) {
    let switcher = switchers[i];

    switcher.onclick = function (e) {
      let themeName = switcher.attributes['data-theme'].value;
      document.body.className = themeName;
      setCookie('theme', themeName, themeCookieTTL);
    }
  }
}

ready(function () {
  fromURL();
  fromButton();
  fromCookie();
});
