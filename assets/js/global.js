function ready (f) {
  /in/.test(document.readyState) ? setTimeout(ready, 9, f) : f()
}

function getUrlParams( prop ) {
  const search = decodeURIComponent( window.location.href.slice( window.location.href.indexOf('?') + 1));
  const definitions = search.split('&');

  let params = {};
  definitions.forEach(function(val, key) {
    let parts = val.split( '=', 2 );
    params[parts[0]] = parts[1];
  });

  return (prop && prop in params) ? params[prop] : params;
}

function setCookie(name,value,days) {
  let expires = "";
  if (days) {
    let date = new Date();
    date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
    expires = "; expires=" + date.toUTCString();
  }

  document.cookie = name + "=" + (value || "")  + expires + "; path=/";
}

function getCookie(name) {
  let cookies = document.cookie.split(";");
  let cookieValue = null;

  for (let i=0; i < cookies.length; i++) {
    let cookie = cookies[i];
    let parts = cookie.split("=");

    if (parts.length === 2) {
      return parts[1];
    }
  }

  return null;
}
