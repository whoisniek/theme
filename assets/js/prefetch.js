ready(function () {
  // Prefetch pages when user hovers over them
  const elements = document.getElementsByTagName('a');
  for (let i = 0, len = elements.length; i < len; i++) {
    let elem = elements[i];
    elem.onmouseover = function () {
      elem.timer = setTimeout(function () {
        let href = elem.getAttribute('href');
        if (href) {
          let link = document.createElement('link');
          link.href = href;
          link.rel = 'prefetch';

          document.getElementsByTagName('head')[0].appendChild(link);
          elem.onmouseover = null;
          elem.onmouseleave = null;
        }
      }, 200);
    };

    elem.onmouseleave = function () {
      clearTimeout(elem.timer);
    }
  }
});
